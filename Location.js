var sha1 = require('sha1') // to hash passwords: hashed = sha1(data)
const sanitizer = require('sanitizer');

const DB = require("./dbHub.js")
db = new DB();

const generator = require('generate-password');
const nodemailer = require('nodemailer');

class Location{

	getLoc(email, callback){
		db.outLocation(email[0], (arr) => {
			if(arr == 1){
				return callback(arr)
			}
			var latitude = arr[0].latitude
			var longitude = arr[0].longitude
			var userLoc = [latitude, longitude]
			return callback(userLoc)
		})
	}

	setLoc(email, lat, lon, callback){
		db.inLocation(email[0], lat, lon, (arr) => {	
			var message = false;
			if(arr == 1 || arr == 2){
				return callback(message)
			}
			message = true;
			return callback(message)
		});
	}

}
module.exports = Location
