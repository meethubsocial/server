var sha1 = require('sha1') // to hash passwords: hashed = sha1(data)
var mysql = require('mysql') // database 
// connect to database
var con = mysql.createConnection({
    host     : 'localhost',        
    user     : 'student',                                                   
    password : 'rtXY3548iy',
    database : 'meethub' 
})

var onlineUsers = []

// Task: verify login parameters 
// Request values : username, password pair 
// Response values: send json object containing {session}

function db(u, p, callback) {
    var sql = 'SELECT * FROM User WHERE Email=? AND Password=?';
    var params = [u, p];

    con.query(sql, params, (err, result, fields) => {
	if (err) throw err;
	
	if(result.length > 0) {
	    console.log( "->: Found User and Pass pair for arguments: u = " + u + " p = " + p);
	    
	    let s = sha1(u+p) // create login session 
	    // check if user already online 
	    for(let i = 0; i < onlineUsers.length; i++) {
		if (onlineUsers[i].session == s) {
		    console.log("WARNING logged in user trying to login")
		    //res.send({session:s}) // change to online
		    return callback(s)
		} // if 
	    } // for
	    // push user to onlineUsers list
	    onlineUsers.push({session:s,username:u,password:p})
	    console.log("->: Added user to list u="+u+" p="+p)
	    console.log("->: Total onlineUsers.length "+onlineUsers.length)
	    // finally send result
	    //res.send(JSON.stringify({session:s}));
	    return callback(s)
	} // if 
	else {
	    console.log( "->: Did not find User and Pass pair for arguments: u="+ u +" p=" + p);
	    //res.send(JSON.stringify({session:"error"}));
	    return callback("error")
	} // else
    }) // con.query
}

function backend(username, password, callback) {
    if(username == 'undefined' || password == 'undefined') {
	console.log("ERROR user or password undefined ")
	return callback(1);
    }
    let u = username
    let p = sha1(password) // hashed password

    return db(u, p, (result)=>{
	return callback(result)
    })
}

module.exports.backend = backend
module.exports.db = db
module.exports.onlineUsers = onlineUsers
