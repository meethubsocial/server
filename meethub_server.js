
// content of index.js
const http = require('http')
var express = require('express') // router
var bodyParser = require('body-parser') // to get data from post requests
var app = express() // required express functionality
var sha1 = require('sha1') // to hash passwords: hashed = sha1(data)

const port = 3000

const User = require("./User");
let user = new User();

const Locat = require("./Location");
let loc = new Locat();

//const DB = require("./dbScripts.js")
const logins = require("./login.js")
const util = require("./utility.js")
//let db = new DB();

// required for the body parser
app.use(bodyParser.urlencoded({ extended: false })) // middleware do not touch
app.use(bodyParser.json()) // middleware do not touch

// Author: Giorgi 
function login(req,res) {   
    console.log("DEBUG 1: Starting: app.post('/login', req, res)=>{...})");    
    console.log("->: User " + req.body.username)
    console.log("->: Pass " + req.body.password)

    user.verify(req.body.username, req.body.password, (result) =>{
	logins.onlineUsers.push({session:result});

	res.send({session:result})
    })
}

// Task: login the user
// 
//
app.post('/login',login); // app.post

// Task: log online user out 
// Request values : session 
// Response values: send json object containing {status}
// Author Giorgi Osadze
app.post('/logout', (req, res) => {
    console.log("DEBUG 2: app.post('/logout', (req, res)=> {...}")
    
    if( req.body.session == undefined ) {
	console.log("WARNING: undefined session")
	res.send({status:'error'})
	return 1;
    }
    // remove user from online user collection
    if( util.rus(req.body.session, logins.onlineUsers) === 0 ) {
	res.send({status:'okay'})
    } 
    else {
	console.log("WARNING: did not find session: "+req.body.session)
	res.send({status:'error'})
    }
}); // logout


// Task: verify login parameters 
// Request values : username, password pair 
// Response values: send json object containing {session}
app.post('/register', (req, res) =>{
   console.log("DEBUG 3: app.post('/register', (req, res)=> {...}");
   user.registration(req.body.username, req.body.password, req.body.fname, req.body.lname, req.body.email, req.body.birth, (result)=>{
	if(!(result === 0)){
		console.log("->: Sending successful result to app...");		
    		user.verify(result[0], result[1], (result) =>{
			logins.onlineUsers.push({session:result});
			res.send({result, success:true})
    		})
		//res.send({success: true});
	}
	else{
		console.log("->: Sending failed result to app...");
		res.send({success: false});
	}
   })
}); // register

app.post('/reset', (req, res) =>{
   user.reset(req.body.email, (result)=>{
	if(result == 0)
		res.send({success:true})
	else{
		res.send({success:false})
	}
   })
}); // reset route

// Task: update the users location values 
// Request values : session, latitude, longitude 
// Response values: status
app.post('/updateloc', (req, res) =>{
    console.log("DEBUG 5: app.post('/updateloc', (req, res)=> {...}");

    util.rui(req.body.session, logins.onlineUsers, (array)=>{
	profile = array;
    })
    if(profile == 0){
	console.log("->: Location update failure...");
	res.send({success: false});
    }

    loc.setLoc(profile, req.body.latitude, req.body.longitude, (result)=> {
	console.log("->: Location update success...");
        res.send({success: true});
    })

    /*// find user and place index into u 
    let u = util.fus(req.body.session) != -1 
    
    // 
    if(u != -1) { 
	logins.onlineUsers[u].lat = req.body.lat;
	logins.onlineUsers[u].lat = req.body.lon;
    }
    console.log(req.body.lat)
    console.log(req.body.lon)*/
}); // updateloc


app.post('/viewProfile', (req, res) => {

	user.viewProfile(profile, (query) => {
		if(query == 1){
			res.send({success:false});
		} 
		else{
			res.send({query, success:true});
		}		
	});	

});	

app.post('/getLoc', (req, res) => {	
    util.rui(req.body.session, logins.onlineUsers, (array)=>{
	profile = array;
    })
    if(profile == 0){
	console.log("->: Location update failure...");
	res.send({success: false});
    }
    loc.getLoc(profile, (res) => {
	res.send({success: res});
    });
});

// Task: Update password from backend
// Author: Ronen(Main), Eric(session, needed for backend class)
app.post('/updatePassword', (req, res) => {
   console.log("DEBUG 6: app.post('/updatePassword', (req, res) => {...}");
   var profile;
   util.rui(req.body.session, logins.onlineUsers, (array)=>{
	profile = array;
   })
   if(profile == 0){
	res.send({success: false});
   }
   user.passwordReset(req.body.oldPassword, req.body.Password, profile, (result)=> {
	if (result == 0) {
		console.log("Update password succeeded");
		
		res.send({success: true});
	}
	else {
		res.send({success: false, error: result});
	}
   })

});

// Task: Update profile picture
// Author: Ronen(Main), Eric(session, needed for backend class)
app.post('/updatePicture', (req, res) => {
   console.log("DEBUG 7: app.post('/updatePicture', (req, res) => {...}");
   var profile;
   util.rui(req.body.session, logins.onlineUsers, (array)=>{
	profile = array;
   })
   if(profile == 0){
	res.send({success: false});
   }
   user.pictureChange(profile, req.body.picture, (result)=> {
        if (result == 0) {
                console.log("Update profile picture succeeded");
                res.send({success: true});
        }
        else {
                res.send({success: false, error: result});
        }
   })

});

// Task: Update Bio
// Author: Ronen(Main), Eric(session, needed for backend class)
app.post('/updateBio', (req, res) => {
   console.log("DEBUG 8: app.post('/updateBio', (req, res) => {...}");
   var profile;
   util.rui(req.body.session, logins.onlineUsers, (array)=>{
	profile = array;
   })
   if(profile == 0){
	res.send({success: false});
   }
   user.bioChange(profile, req.body.bio, (result)=> {
        if (result == 0) {
                console.log("Update bio succeeded");
                res.send({success: true});
        }
        else {
                res.send({success: false, error: result});
        }
   })

});

// This route is to test the hub locations
app.post('/dummy', (req, res) => {
    var hubs = [{ lat: 43.72, long: -79.41, radius: 1000, id:1 }, 
		{ lat: 43.74, long: -79.33, radius: 1000, id:2 },
		{ lat: 43.76, long: -79.51, radius: 1000, id:3 },
		{ lat: 43.78, long: -79.35, radius: 1000, id:4 },
		{ lat: 43.80, long: -79.58, radius: 1000, id:5 },
		{ lat: 43.82, long: -79.48, radius: 1000, id:6 },
		{ lat: 43.84, long: -79.31, radius: 1000, id:7 },
		{ lat: 43.86, long: -79.45, radius: 1000, id:8 },
		{ lat: 43.88, long: -79.35, radius: 1000, id:9 },
		{ lat: 43.90, long: -79.30, radius: 1000, id:10 },
		{ lat: 43.92, long: -79.39, radius: 1000, id:11 }
	       ]

    res.send(hubs)
});

app.listen(3000);
