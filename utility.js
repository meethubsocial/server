// Giorgi Osadze
// remove user session from collection
// s = session to remove
// c = colleciton 
// 0 if okay, -1 if no match
function rus(s, c) {
    // find if user is online
    for(let i = 0; i < c.length; i++) {
	if(c[i].session == s) { 
	    console.log("->: User: "+c[i].username+" removed")
	    c.splice(i, 1)
	    return 0
	} // if 
    } // for 
    return -1
}
// Giorgi Osadze
// find user session from collection
// s = session to find 
// c = collection 
// 0 if found, -1 if no match
function fus(s, c) {
    // find if user is online
    for(let i = 0; i < c.length; i++) {
	if(c[i].session == s) { 
	    console.log("->: User: "+c[i].username+" found ")
	    return i
	} // if 
    } // for 
    return -1
}

// Eric Ferguson
// Get array of Username and Password
// s = session, c = collection
// return array else 0
function rui(s,c, callback) {
    for(let i = 0; i < c.length; i++) {
	if(c[i].session == s) { 
	    console.log("->: User: "+c[i].username+" found ")
	    var user = [c[i].username, c[i].password];
	    return callback(user);
	} // if 
    } // for 
    return 0
}
// Giorgi Osadze
// get user session json
// s = session to find 
// c = collection 
// {user json} if found, -1 if no match
function gu(s, c) {
    // find if user is online
    for(let i = 0; i < c.length; i++) {
	if(c[i].session == s) { 
	    console.log("->: User: "+c[i].username+" found ")
	    return c[i]
	} // if 
    } // for 
    return -1
}

module.exports.rus = rus
module.exports.fus = fus
