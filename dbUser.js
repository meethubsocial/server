// Coded by Ronen Agarunov
// Database relations with user profile

// +++-***-+++ File will be continuously updated +++-***-+++
// Last updated: 2/23/2018

// Connection to database (MySQL)
var mysql      = require('mysql')
var sha1       = require('sha1')
var con = mysql.createConnection({
    host     : 'localhost',
    user     : 'student',
    password : 'rtXY3548iy',
    database : 'meethub'
});
var onlineUsers = []
class DB{
    login(u, p, callback){
	
	var sql = 'SELECT * FROM User WHERE Email=? AND Password=?';
	var params = [u, p];

	con.query(sql, params, (err, result, fields) => {
	    if (err) throw err;
	    
	    if(result.length > 0) {
		console.log( "->: Found User and Pass pair for arguments: u = " + u + " p = " + p);
		
		let s = sha1(u+p) // create login session 
		// check if user already online 
		for(let i = 0; i < onlineUsers.length; i++) {
		    if (onlineUsers[i].session == s) {
			console.log("WARNING logged in user trying to login")
			//res.send({session:s}) // change to online
			return callback(s)
		    } // if 
		} // for
		// push user to onlineUsers list
		onlineUsers.push({session:s,username:u,password:p})
		console.log("->: Added user to list u=" + u + " p=" + p)
		console.log("->: Total onlineUsers.length " + onlineUsers.length)
		// finally send result
		//res.send(JSON.stringify({session:s}));
		return callback(s)
	    } // if 
	    else {
		console.log( "->: Did not find User and Pass pair for arguments: u=" + u + " p=" + p);
		//res.send(JSON.stringify({session:"error"}));
		return callback("error")
	    } // else
	}) // con.query
	

    } // login

    register(Username, Password, First_Name, Last_Name, Email, DoB, callback) {

	var check = "SELECT * FROM User WHERE Email=?";
	var pCheck = [Email];
	con.query(check, pCheck, function(err, results, fields) {
		
		if (results.length > 0) {
			console.log("Registration failed, account with email exists");
			return callback(2);
		}
		else {
	   		var sql = "INSERT INTO User (Username, Password, First_Name, Last_Name, Email, Picture, Bio, dateOfBirth) VALUES(?, ?, ?, ?, ?, \"default.jpg\", \"Hi! I am a new member of MeetHub\", ?)";
	    		var params = [Username, Password, First_Name, Last_Name, Email, DoB];
	    		con.query(sql, params, function (err, results, fields) {
				if (err) {
			 		console.log("Registration failed in DB: " + err);
		    			return callback(1);
                		}
				else {
		    			console.log("registration successfully completed for: " + Username);
		    			return callback(0);
				}
	    		});
		}
	});
    }

    recovery(Email, tempPassword, callback) {
	    var sql = "UPDATE User SET Password=? WHERE Email=?";
	    var params = [tempPassword, Email];
	    con.query(sql, params, function(err, results, fields) { 
        	    if(err){
			console.log("->: Password reset failed in DB " + err);
   			return callback(1);
                    }
                    else{
	    		console.log("->: Password updated to hash of " + tempPassword)
			return callback(0);
		    }
	    });
    }

    /* 
    // Profile update sections
    */
    updatePassword(Email, oldPass, newPass, callback) {

	var sql = "SELECT * FROM User WHERE Email=? AND Password=?";
	var params = [Email, oldPass];
	con.query(sql, params, function(err, result, fields) {

		    if (err) throw err;

		    if (result.length > 0) {
			
			var sqlChange = "UPDATE User SET Password=? WHERE Email=?";
			var paramsAnother = [newPass, Email];
			con.query(sqlChange, paramsAnother, function(err, result, fields) {
				if (err) {
					console.log("Password change failed in update to new password stage" + err);
					return callback(1);
				}
				else {
					console.log("Password successfully changed to hash of " + newPass);
					return callback(0);
				}
			});
		     }

		     else {
			console.log("Old password is incorrect for: " + Email);
			return callback(2);			
	    	    }
      });
    }

    updatePicture(Email, picUrl, callback) {

	var sql = "UPDATE User SET Picture=? WHERE Email=?";
	var params = [picUrl, Email];

	con.query(sql, params, function(err, results, fields) {
                    if(err){
                        console.log("Picture update failed in DB " + err);
                        return callback(1);
                    }
                    else{
                        console.log("Picture update succeeded");
                        return callback(0);
                    }
        });
   }

   updateBio(Email, Bio, callback) {
	
	var sql = "UPDATE User SET Bio=? WHERE Email=?";
	var params = [Bio, Email];

 	con.query(sql, params, function(err, results, fields) {
                    if(err){
                        console.log("Bio update failed in DB " + err);
                        return callback(1);
                    }
                    else{
                        console.log("Bio update succeeded");
                        return callback(0);
                    }
        });
   }

   viewProfile(Email) {

	var sql = "SELECT First_Name, Last_Name, Picture, Bio, dateOfBirth FROM User WHERE Email=?";
	var params = [Email];

	 con.query(sql, params, function(err, results, fields) {
                    if(err){
                        console.log("View profile failed in DB " + err);
                        return callback(1);
                    }
                    else{
                        console.log("View profile succeeded ");
                        return callback(results);
                    }
        });	
   }

   /*
   // Hubs
   */

};


//Experimentation with Giorgi & I (Ronen):
//var k = new DB()
//k.register('ronen', 'pass', 'Ronen', 'Aga', 'ronen@myspace.com', 'url.jpg', 'this is my bio');
//k.login('ronen@myspace.com', 'pass');
/*
var t = new DB()
t.viewProfile('ronen@myspace.com');
module.exports = DB;
*/
